# A.31 Firmwares
FIRMWARE_IMAGES := \
    abl \
    bluetooth \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    modem \
    multiimgoem \
    oplus_sec \
    oplusstanvbk \
    qupfw \
    rpm \
    splash \
    tz \
    uefisecapp \
    xbl \
    xbl_config

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
